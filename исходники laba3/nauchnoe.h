#ifndef NAUCHNOE_H
#define NAUCHNOE_H
#include "baza.h"
/**
����� ������� �������
*/
class nauchnoe : public baza{
private:
	string* nazvaniya_kursov;		//�������� ������, ������������ ��� ������� (�� 1 �� 3)
protected:
	/**
	����� ���������� �� ������� �������
	*/
	ostream& show(ostream&)const;
	/**
	���� ���������� �� ������� �������
	*/
	istream& get(istream&);
public:
	/**
	���������������� ����������
	*/
	nauchnoe() :nazvaniya_kursov(NULL){};
	/**
	���������� �����������
	*/
	nauchnoe(const nauchnoe &nauch){
		setfio(nauch.getfio());
		setnazvanie(nauch.getnazvanie());
		setyear(nauch.getyear());
		setizd(nauch.getizd());
		setkolvo(nauch.getkolvo());
		nazvaniya_kursov = nauch.nazvaniya_kursov;
	};
	/**
	������������� �������� ������������
	*/
	nauchnoe& operator = (const nauchnoe &nauch){
		delete[]nazvaniya_kursov;
		setfio(nauch.getfio());
		setnazvanie(nauch.getnazvanie());
		setyear(nauch.getyear());
		setizd(nauch.getizd());
		setkolvo(nauch.getkolvo());
		nazvaniya_kursov = nauch.nazvaniya_kursov;

	};
	/**
	������������ ������
	*/
	virtual nauchnoe* clone() const
	{
		return new nauchnoe(*this);
	}
	/**
	����������� ���� �������
	*/
	int type()const;
	/**
	�������� ��������
	*/
	string getname(int)const;
	/**
	�������� �������� ������
	*/
	void izm(int kolvo, const string*);
	/**
	�������� ������ ��������
	*/
	string* get_nazvaniya_kursov()const{
		return nazvaniya_kursov;
	}
	/**

	*/
	virtual string get_tema()const{ return 0; };


};

#endif // NAUCHNOE_H
