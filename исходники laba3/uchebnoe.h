#ifndef UCHEBNOE_H
#define UCHEBNOE_H

#include "baza.h"

/**
����� ������� �������
*/
class uchebnoe : public baza{
private:
	string nazvanie_kursa;		//�������� �����
	int* ind_grupp;			//�������� �������� ����� (�� 1 �� 8)
protected:
	/**
	����� ���������� �� ������� �������
	*/
	ostream& show(ostream&) const;
	/**
	���� ���������� �� �������
	*/
	istream& get(istream&);
public:
	/**
	������ ���������������� �����������
	*/
	uchebnoe() :baza(), ind_grupp(NULL){ nazvanie_kursa.clear(); };

	/**
	����������� � �����������
	*/

	uchebnoe(const uchebnoe &uch){
		setfio(uch.getfio());
		setnazvanie(uch.getnazvanie());
		setyear(uch.getyear());
		setizd(uch.getizd());
		setkolvo(uch.getkolvo());

		nazvanie_kursa = uch.nazvanie_kursa;
		ind_grupp = uch.ind_grupp;
	}
	/**
	������������� �������� ������������
	*/
	uchebnoe& operator = (const uchebnoe &uch){
		delete[]ind_grupp;
		setfio(uch.getfio());
		setnazvanie(uch.getnazvanie());
		setyear(uch.getyear());
		setizd(uch.getizd());
		setkolvo(uch.getkolvo());

		nazvanie_kursa = uch.nazvanie_kursa;
		ind_grupp = uch.ind_grupp;
	}
	/**
	�������� �������� ������
	*/
	string* get_nazvaniya_kursov()const{ return 0; }
	/**
	�������� ����
	*/
	string get_tema()const { return 0; }
	/**
	����������� ����� ������������ ������
	*/
	virtual uchebnoe* clone() const
	{
		return new uchebnoe(*this);
	}

	/**
	����������� ���� ������
	*/
	int type()const;
	/**
	�������� ��������
	*/
	string getname()const;
	/**
	�������� ��������
	*/
	void setname(string);
	/**
	�������� �������� �������� �����
	*/
	void izm(int kolvo, const int*);
	/**
	���������� �������� ������
	*/
	virtual ~uchebnoe(){
		nazvanie_kursa.clear();
	}
};




#endif
