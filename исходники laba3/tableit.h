#ifndef TABLEIT_H
#define TABLEIT_H

#include "baza.h"

class TableIt{
private:
	map<const int, baza *>::iterator cur;
public:
	TableIt(){}
	TableIt(map<const int, baza *>::iterator it) :cur(it){}
	int operator !=(const TableIt &) const;
	int operator ==(const TableIt &) const;
	pair<const int, baza *> & operator *();
	TableIt & operator ++();
	TableIt operator ++(int);
};





#endif // TABLEIT_H
