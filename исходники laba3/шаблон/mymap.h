
#include <iostream>
using namespace std;

// -------- template struct Pair ----------------
template <class IND, class INF>
struct Pair{
	IND first;
	INF second;
	Pair() :first(IND()), second(nullptr){}
	Pair(const Pair &p) :first(p.first), second(p.second){}
};



// -------- template class Mymap ----------------
template <class IND, class INF>
class MymapIt;

template <class IND, class INF>
class Mymap{
	friend class MymapIt<IND, INF>;
private:
	static const int QUOTA = 10;
	int cnt,cur;
	Pair<IND, INF> *arr;
	int getPos(const IND &) const; 
public:
	Mymap() :cnt(QUOTA), cur(0), arr(new Pair<IND, INF>[QUOTA]){}
	Mymap(const Mymap<IND, INF> &);
	~Mymap(){ delete[] arr; }
	Mymap<IND, INF>& operator = (const Mymap<IND, INF> &);

	baza* Mymap::operator [](const int &s) const{
		int i = getPos(s);
			if (i==-1)
				throw exception("������!");
			else
				return arr[i].second;

		

	}
	


	void insert(const IND&, const INF&);
	void erase(const IND&);


	typedef MymapIt<IND, INF> Iterator;

	Iterator find(const IND&) const;
	Iterator begin();
	Iterator end();
};

template <class IND, class INF>
Mymap<IND, INF>::Mymap(const Mymap<IND, INF> &a) :cnt(a.cnt), cur(a.cur), arr(new Pair[a.cnt])
{
	for (int i = 0; i < cnt; i++){
		arr[i].first = a.arr[i].first;
		arr[i].second = a.arr[i].second->clone();
	}
}

template <class IND, class INF>
Mymap<IND, INF>& Mymap<IND, INF>::operator = (const Mymap<IND, INF> &a)
{
	if (this != &a){
		delete[] arr;
		arr = new Pair<IND, INF>[cnt = a.cnt];
		cur = a.cur;
		for (int i = 0; i < cnt; i++){
			arr[i].first = a.arr[i].first;
			arr[i].second = a.arr[i].second->clone();
		}
	}
	return *this;
}

template <class IND, class INF>
int Mymap<IND, INF>::getPos(const IND &s) const
{
	for (int i = 0; i < cur; ++i)
	if (arr[i].first == s)
		return i;
	return -1;
}

template <class IND, class INF>
void Mymap<IND, INF>::insert(const IND &s, const INF &p)
{
	int i = getPos(s);
	if (i < 0){
		i = cur;
		if (cur >= cnt){
			Pair<IND, INF> *old = arr;
			arr = new Pair<IND, INF>[cnt += QUOTA];
			for (i = 0; i < cur; ++i)
				arr[i] = old[i];
			delete[] old;
		}
		arr[cur].first = s;
		++cur;
	}
	else
		delete arr[i].second;
	arr[i].second = p->clone();
}

template <class IND, class INF>
void Mymap<IND, INF>::erase(const IND &s)
{
	int i = getPos(s);
	if (i < 0){
		throw exception("������!");
	}
	else
	{
		//arr[i].first = NULL;
		delete arr[i].second;
		arr[i].second = NULL;
		arr[i].first = NULL;
		

	}
}


template <class IND, class INF>
MymapIt<IND, INF> Mymap<IND, INF>::begin()
{
	return MymapIt<IND, INF>(this->arr);
}

template <class IND, class INF>
MymapIt<IND, INF> Mymap<IND, INF>::end()
{
	return MymapIt<IND, INF>(this->arr + cur);
}

template <class IND, class INF>
MymapIt<IND, INF> Mymap<IND, INF>::find(const IND &s) const
{
	int i = getPos(s);
	if (i < 0)
		i = cur;
	return MymapIt<IND, INF>(this->arr + i);
}


// -------- template for class AssocIt ----------------
template <class IND, class INF>
class MymapIt{
private:
	Pair<IND, INF> *cur;
public:
	MymapIt() :cur(0){}
	MymapIt(Pair<IND, INF> *a) :cur(a){}
	int operator !=(const MymapIt<IND, INF> &) const;
	int operator ==(const MymapIt<IND, INF> &) const;
	Pair<IND, INF>& operator *();
	MymapIt<IND, INF>& operator ++();
	MymapIt<IND, INF> operator ++(int);
};

template <class IND, class INF>
int MymapIt<IND, INF>::operator !=(const MymapIt<IND, INF> &it) const
{
	return cur != it.cur;
}

template <class IND, class INF>
int MymapIt<IND, INF>::operator ==(const MymapIt<IND, INF> &it) const
{
	return cur == it.cur;
}

template <class IND, class INF>
Pair<IND, INF>& MymapIt<IND, INF>::operator *()
{
	return *cur;
}

template <class IND, class INF>
MymapIt<IND, INF>& MymapIt<IND, INF>::operator ++()
{
	++cur;
	return *this;
}

template <class IND, class INF>
MymapIt<IND, INF> MymapIt<IND, INF>::operator ++(int)
{
	MymapIt<IND, INF> res(*this);
	++cur;
	return res;
}
