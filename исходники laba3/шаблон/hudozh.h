#ifndef HUDOZH_H
#define HUDOZH_H

#include "baza.h"


/**
����� �������������� �������
*/
class hudozh : public baza{
private:
	string tema;		//�������� ������������� ������������
protected:
	/**
	����� ���������� � �������������� �������
	*/
	ostream& show(ostream&)const;
	/**
	���� ���������� � �������������� �������
	*/
	istream& get(istream&);
public:
	/**
	���������������� �����������
	*/
	hudozh() { tema.clear(); }
	/**
	���������� �����������
	*/
	//    hudozh(const hudozh &hud){
	//        setfio(hud.getfio());
	//        setnazvanie(hud.getnazvanie());
	//        setyear(hud.getyear());
	//        setizd(hud.getizd());
	//        setkolvo(hud.getkolvo());
	//        tema = hud.tema;

	//    }
	/**
	������������� �������� ������������
	*/
	hudozh& operator = (const hudozh &hud){

		setfio(hud.getfio());
		setnazvanie(hud.getnazvanie());
		setyear(hud.getyear());
		setizd(hud.getizd());
		setkolvo(hud.getkolvo());
		tema = hud.tema;
		return (*this);
	}
	/**

	*/
	string* get_nazvaniya_kursov()const{ return 0; }
	/**
	������������ ������
	*/
	virtual hudozh* clone() const
	{
		return new hudozh(*this);
	}
	/**
	�������� ��� �������
	*/
	int type()const;
	/**
	�������� ����
	*/
	string gettema()const;
	/**
	�������� ����
	*/
	void settema(string);
	/**
	�������� ����
	*/
	string get_tema()const{ return tema; }
	/**
	���������� ��������������� �������
	*/
	virtual ~hudozh(){}
};


#endif // HUDOZH_H
