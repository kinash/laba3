#include "hudozh.h"

int hudozh::type()const{
	return 3;
}

string hudozh::gettema()const{
	return tema;
}

void hudozh::settema(string ntema){
	tema = ntema;
}

//int kolvo_kur;
//int kolvogrupp;


//����� ���������� � �������������� �������
ostream& hudozh::show(ostream& os)const
{
	os << "���: " << getfio() << endl <<
		"��������: " << getnazvanie() << endl <<
		"���: " << getyear() << endl <<
		"������������: " << getizd() << endl <<
		"����������: " << getkolvo() << endl <<
		"����:" << gettema();
	return os;
}


//���� ���������� � �������������� �������
istream& hudozh::get(istream& is){
	string fio;
	string nazvanie;
	int year;
	string izd;
	int kolvo;
	string nazvanie_kursa;
	string tema;

	cout << "������� ���, ��������, ������������, ���, ���-�� � ����: " << endl;
	is >> fio >> nazvanie >> izd >> year >> kolvo >> tema;

	setfio(fio);
	setnazvanie(nazvanie);
	setyear(year);
	setizd(izd);
	setkolvo(kolvo);
	settema(tema);

	return is;
}
