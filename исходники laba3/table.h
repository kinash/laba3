#ifndef TABLE_H
#define TABLE_H

#include "baza.h"

//����� �������
class Table{
private:
	map<const int, baza *> arr;		//���������, ��������� �� ����� ������� � ��������� �� ��������� �������
public:
	/**
	���������������� �����������
	*/
	Table(){}
	/**
	���������� �����������
	*/
	Table(const Table &);
	/**
	������������� �������� ������������
	*/
	Table& operator = (const Table &);
	/**
	������� � �������
	*/
	void insert(const int &, const baza *);
	/**
	�������� �� �������
	*/
	void udal(const int&);
	/**
	������������� �������� []
	*/
	baza* operator [](const int &) const;

	friend class TableIt;
	typedef TableIt Iterator;
	Iterator begin();
	/**

	*/
	Iterator end();
	/**
	����� � �������
	*/
	Iterator find(const int &);
	/**
	���������� �������
	*/
	virtual ~Table()
	{
		map<const int, baza *>::iterator p;
		for (p = arr.begin(); p != arr.end(); ++p){
			delete p->second;
		}
	}

};

#endif // TABLE_H
