#include "nauchnoe.h"

int nauchnoe::type()const{
	return 2;
}

string nauchnoe::getname(int nomer)const{
	return nazvaniya_kursov[nomer];
}

void nauchnoe::izm(int kolvo, const string *mas){
	if ((kolvo > 0) || (mas != NULL)){
		delete[] nazvaniya_kursov;
		nazvaniya_kursov = new string[3];
		for (int i = 0; i < kolvo; i++){
			nazvaniya_kursov[i] = mas[i];
		}
	}
	else throw exception("������!");
}


//����� ���������� � ������� �������
ostream& nauchnoe::show(ostream& os)const
{
	os << "���: " << getfio() << endl <<
		"��������: " << getnazvanie() << endl <<
		"���: " << getyear() << endl <<
		"������������: " << getizd() << endl <<
		"����������: " << getkolvo() << endl <<
		"�������� ������:";
	for (int i = 0; i<3; i++)
	{

		if (!nazvaniya_kursov[i].empty())
			os << nazvaniya_kursov[i] << "  ";
	}
	;
	return os;
}

//���� ���������� � ������� �������
istream& nauchnoe::get(istream& is){
	string fio;
	string nazvanie;
	int year;
	string izd;
	int kolvo;
	int kolvokur;

	cout << "������� ���, ��������, ������������, ��� � ���-��: " << endl;
	is >> fio >> nazvanie >> izd >> year >> kolvo;

	cout << "������� ���-�� ������(1-3): " << endl;
	cin >> kolvokur;
	string *mass = new string[3];

	cout << "������� �������� ������: " << endl;
	for (int i = 0; i < kolvokur; i++){
		if (kolvokur>3 || kolvokur < 0){
			break;
		}
		cin >> mass[i];
	}
	setfio(fio);
	setnazvanie(nazvanie);
	setyear(year);
	setizd(izd);
	setkolvo(kolvo);
	izm(kolvokur, mass);

	return is;
}
