#include "tableit.h"
#include "table.h"

int Table::Iterator::operator !=(const TableIt &it) const
{
	return cur != it.cur;
}

int TableIt::operator ==(const TableIt &it) const
{
	return cur == it.cur;
}

pair<const int, baza *> & TableIt::operator *()
{
	return *cur;
}

TableIt & TableIt::operator ++()
{
	++cur;
	return *this;
}

TableIt TableIt::operator ++(int)
{
	TableIt res(*this);
	++cur;
	return res;
}


