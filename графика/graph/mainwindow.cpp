#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../iskh/baza.h"
#include "../iskh/baza.cpp"
#include "../iskh/hudozh.h"
#include "../iskh/hudozh.cpp"
#include "../iskh/uchebnoe.h"
#include "../iskh/uchebnoe.cpp"
#include "../iskh/nauchnoe.h"
#include "../iskh/nauchnoe.cpp"

#include <sstream>
#include <QDebug>
#include <QProcess>
#include <QTextStream>
#include <ostream>
#include <fstream>

Mymap<int, baza*> tb;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->oknovivoda->setReadOnly(true);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_clicked()   // ���������� ���������������
{
    string fio, izd, nazvanie, tema;


    int kolvo, year, shifr;
    baza *ptr = NULL;
    hudozh hud;


    shifr=ui->lineEdit->text().toInt();
    fio=ui->lineEdit_2->text().toStdString();
    izd=ui->lineEdit_3->text().toStdString();
    nazvanie=ui->lineEdit_4->text().toStdString();
    tema=ui->lineEdit_5->text().toStdString();
    year=ui->lineEdit_6->text().toInt();
    kolvo=ui->lineEdit_7->text().toInt();


    hud.setfio(fio);
    hud.setizd(izd);
    hud.setnazvanie(nazvanie);
    hud.settema(tema);
    hud.setyear(year);
    hud.setkolvo(kolvo);

    ptr=&hud;
    tb.insert(shifr, ptr);
    if (shifr)
        QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������!"));
    else
        QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));

}

void MainWindow::on_pushButton_3_clicked()  //���������� ��������
{
    string fio, izd, nazvanie, nazv_kursa;

    int kolvo, year, shifr,ind_gr;
    baza *ptr = NULL;
    uchebnoe uch;

    shifr=ui->lineEdit_8->text().toInt();
    fio=ui->lineEdit_13->text().toStdString();
    izd=ui->lineEdit_14->text().toStdString();
    nazvanie=ui->lineEdit_9->text().toStdString();
    nazv_kursa=ui->lineEdit_11->text().toStdString();
    year=ui->lineEdit_12->text().toInt();
    kolvo=ui->lineEdit_10->text().toInt();
    ind_gr=ui->lineEdit_15->text().toInt();

    uch.setfio(fio);
    uch.setizd(izd);
    uch.setnazvanie(nazvanie);
    uch.setname(nazv_kursa);
    uch.setyear(year);
    uch.setkolvo(kolvo);
    uch.izm2(ind_gr);

    ptr=&uch;
    tb.insert(shifr, ptr);
    if (shifr)
        QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������!"));
    else
        QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));
}




ostream & operator <<(ostream &os, const Pair<int, baza *> &p) //���� ������
{
    cout << endl;
    if (p.second)
        os  << p.first << endl;
    if (p.second)
        os  << (*(p.second));
    //else
    //	os << "is empty";
    cout << endl;
    return os;
}



void MainWindow::on_pushButton_2_clicked()  //����� �������
{
    ui->oknovivoda->clear();
    ui->oknovivoda->append("=========================================TABLE==========================================");

    string str;
    QString myString;
    QString myString2;

    baza *pSh = NULL;
//    hudozh hud;

    Mymap<int, baza *>::Iterator p;
//    for (p = tb.begin(); p != tb.end(); ++p)
//        if ((*p).first!=NULL){
//            cout << (*p);// << endl;
//        }
    for (p = tb.begin(); p != tb.end(); ++p)
        if ((*p).first!=NULL){
            pSh=(*p).second;
            str=pSh->frm();
            myString2=QString::fromLocal8Bit("����: ")+QString::number((*p).first);
            ui->oknovivoda->append(myString2);
            myString = QString::fromStdString(str);
            ui->oknovivoda->append(myString);
          //  ui->oknovivoda->append(QString::fromLocal8Bit(myString));
        }

//    for (p = tb.begin(); p != tb.end(); ++p)
//        if ((*p).first!=NULL){
//            pSh = (*p).second;
//            g=pSh->getfio();
//        }





//   QMessageBox::information(this, "vivod", "vivod!");
    QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������� ��� ���� ���������� ��������!"));

}



void MainWindow::on_pushButton_4_clicked()  //���������� ��������
{

        string fio, izd, nazvanie, nazv_kursa;

        int kolvo, year, shifr;
        baza *ptr = NULL;
        nauchnoe nauch;

        shifr=ui->lineEdit_21->text().toInt();
        fio=ui->lineEdit_23->text().toStdString();
        izd=ui->lineEdit_16->text().toStdString();
        nazvanie=ui->lineEdit_20->text().toStdString();
        nazv_kursa=ui->lineEdit_17->text().toStdString();
        year=ui->lineEdit_22->text().toInt();
        kolvo=ui->lineEdit_19->text().toInt();

        nauch.setfio(fio);
        nauch.setizd(izd);
        nauch.setnazvanie(nazvanie);
        nauch.setnazvkur(nazv_kursa);
        nauch.setyear(year);
        nauch.setkolvo(kolvo);

        ptr=&nauch;
        tb.insert(shifr, ptr);
        if (shifr)
            QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������!"));
        else
            QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));

}

void MainWindow::on_pushButton_5_clicked()    // ��������� � ����
{
            string filename;
            filename=ui->lineEdit_18->text().toStdString();

            ofstream f;
            f.open(filename, ios::binary);
            Mymap<int, baza *>::Iterator it;
            for (it = tb.begin(); it != tb.end(); ++it)
            {
                f << (*it).second->type()<<endl;
                f << (*it) << endl;
            }
            f.close();
            QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������!"));

}

void MainWindow::on_pushButton_6_clicked()  // ���������
{
    string filename;
    filename=ui->lineEdit_18->text().toStdString();

    ifstream f(filename);
    if (!f)
    {
        QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("���� �� ������!"));


    }
    else{
    while (!f.eof()){
        baza *ptr;
        uchebnoe uch;
        nauchnoe nauch;
        hudozh hud;

        int type, shifr, year, kolvo, ind_gr;
        string fio, nazvanie, izdatelstvo, tema, nazvanie_kursa;

        f >> type;
        switch (type){
        case 1:
            f >> shifr;
            f >> fio;
            f >> nazvanie;
            f >> year;
            f >> izdatelstvo;
            f >> kolvo;
            f >> nazvanie_kursa;
            f >> ind_gr;

            uch.setfio(fio);
            uch.setizd(izdatelstvo);
            uch.setkolvo(kolvo);
            uch.setname(nazvanie_kursa);
            uch.setnazvanie(nazvanie);
            uch.setyear(year);
            uch.izm2(ind_gr);
            ptr = &uch;
            break;
        case 2:
            f >> shifr;
            f >> fio;
            f >> nazvanie;
            f >> year;
            f >> izdatelstvo;
            f >> kolvo;
            f >> nazvanie_kursa;

            nauch.setfio(fio);
            nauch.setizd(izdatelstvo);
            nauch.setkolvo(kolvo);
            nauch.setnazvanie(nazvanie);
            nauch.setyear(year);
            nauch.setnazvkur(nazvanie_kursa);
            ptr = &nauch;
            break;
        case 3:
            f >> shifr;
            f >> fio;
            f >> nazvanie;
            f >> year;
            f >> izdatelstvo;
            f >> kolvo;
            f >> tema;

            hud.settema(tema);
            hud.setfio(fio);
            hud.setizd(izdatelstvo);
            hud.setkolvo(kolvo);
            hud.setnazvanie(nazvanie);
            hud.setyear(year);
            ptr = &hud;
            break;
        }
        tb.insert(shifr, ptr);
        //tb[shifr] = ptr->clone();
        cout << endl;
        tema.clear();
        fio.clear();
        izdatelstvo.clear();
        kolvo = NULL;
        nazvanie.clear();
        year = NULL;
        shifr = NULL;
        ind_gr = NULL;
        nazvanie_kursa.clear();
    }
    QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("���������!"));
    }
}

int findmas(string mas[10], string stroka)
    {
        for (int i = 0; i < 10; i++)
        {
            if (mas[i] == stroka)
                return 1;
        }
        return 0;
    }

void MainWindow::on_pushButton_7_clicked()  //������
{
                int shifr;
                shifr=ui->lineEdit_24->text().toInt();
                if(shifr){
                Mymap<int, baza *>::Iterator it;

                it = tb.find(shifr);
                if (tb[shifr]->getkolvo()>0)
                {
                    tb[shifr]->setkolvo(tb[shifr]->getkolvo() - 1);
                    QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("��������� �����!"));
                }
                else
                    QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("����������� ���!"));
                }
                else QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));
}


void MainWindow::on_pushButton_8_clicked()  //�������
{
    int shifr;
    shifr=ui->lineEdit_25->text().toInt();
    if(shifr){
    Mymap<int, baza *>::Iterator it;

    it = tb.find(shifr);
    tb[shifr]->setkolvo(tb[shifr]->getkolvo() + 1);
    if(tb[shifr]->getkolvo())
    {
        tb[shifr]->setkolvo(tb[shifr]->getkolvo() + 1);
        QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("������� ������ �������!"));
    }
    else QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));
    }
    else QMessageBox::information(this, QString::fromLocal8Bit("������!"), QString::fromLocal8Bit("������!"));
}

void MainWindow::on_pushButton_9_clicked()   //������������ ���-���
{
    ui->textBrowser->clear();
    ui->textBrowser->append(QString::fromLocal8Bit("====================������������ �����������===================="));

    Mymap<int, baza *>::Iterator it;
    Pair<int, baza*> p;
    baza *pSh = NULL;
    int j = 0;
    string massiv[10];
    for (it = tb.begin(); it != tb.end(); ++it){
        p = *it;

        if (p.second != NULL)
        {
            pSh = (*it).second;
        int type = pSh->type();
//			string *mas = pSh->get_nazvaniya_kursov();
        switch (type){

        case 1: if (findmas(massiv, pSh->getnazvkur()) == 0)
        {
                    massiv[j] = pSh->getnazvkur();
                    j++;
        }
                //cout << pSh->getnazvanie() << endl;
                break;

        case 2:
            //for (int i = 0; i < 3; i++){
                if (findmas(massiv, pSh->getnazvkur()) == 0)
                {
                    massiv[j] = pSh->getnazvkur();
                    j++;
                }

                //	cout << mas[i] << endl;
            //}
            break;

        case 3:	break;

        }


    }
    }
    for (int k = 0; k < 10; k++)
    {
//            cout << massiv[k] << endl;
            ui->textBrowser->append(QString::fromStdString(massiv[k]));
    }
    QMessageBox::information(this, QString::fromLocal8Bit("OK!"), QString::fromLocal8Bit("�����, ������������ ���-��� ��������!"));

}
