#-------------------------------------------------
#
# Project created by QtCreator 2014-12-12T20:19:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = graph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../iskh/uchebnoe.cpp \
    ../iskh/nauchnoe.cpp \
    ../iskh/hudozh.cpp \
    ../iskh/baza.cpp

HEADERS  += mainwindow.h \
    ../iskh/uchebnoe.h \
    ../iskh/nauchnoe.h \
    ../iskh/mymap.h \
    ../iskh/hudozh.h \
    ../iskh/baza.h

FORMS    += mainwindow.ui
