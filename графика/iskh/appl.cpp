#include "baza.h"
#include "hudozh.h"
#include "nauchnoe.h"
#include "uchebnoe.h"

#include <fstream>

#include "appl.h"



using namespace std;

const char* filename = "file.txt";
const char *Names[] = { NULL, "�������", "�������", "��������������" };
const char *Menu[] = {
	"1. �������� ����� �������.",
	"2. ������� ��� ����������.",
	"3. ����� �� �����.",
	"4. ������������ ��������.",
	"5. ������ ���������� �������.",
	"6. ������� ���������� �������.",
	"7. ������� ������� �������.",
	"8. ��������� � ����.",
	"9. ��������� �� �����",
	"10. �������� ������",
	"0. �����"
},
*Choice = "������� �����: ",
*Msg = "������! ���������! ";

	const char *Sh[] = { "1. �������", "2. �������", "3. ��������������", "0. Quit" };
	const int NumSh = sizeof(Sh) / sizeof(Sh[0]);
	const int Num = sizeof(Menu) / sizeof(Menu[0]);


Appl::Appl()
{
	fptr[0] = NULL;
	fptr[1] = new CAdd;
	fptr[2] = new CShowAll;
	fptr[3] = new CFind;
	fptr[4] = new CIter;
	fptr[5] = new Cvidacha;
	fptr[6] = new Cvozvrat;
	fptr[7] = new Cudal;
	fptr[8] = new CSaveFile;
	fptr[9] = new CLoadFile;
	fptr[10] = new Cnazv_kursov;

}

Appl::~Appl()
{
	for (int i = 0; i < 10; ++i)
		delete fptr[i];
}

	int Appl::run()
	{
		setlocale(LC_ALL, "Russian");
		int ind;
		while (ind = Answer(Menu, Num)){
			(*fptr[ind])(*this);
		}
		cout << "�����.." << endl;
		return 0;
	}

	int Appl::Answer(const char *alt[], int n)
	{

		int answer;
		string ansbuf;
		const char *prompt = Choice;
		cout << "��� ������: " << endl;
		for (int i = 0; i < n; i++)
			cout << alt[i] << endl;
		do{
			cout << prompt << ": -> ";
			prompt = Msg;

			cin >> answer;
			if (!cin.good()){
				cin.clear();
				fflush(stdin);
				answer = 111;
				cin >> answer;
			}

		} while (answer < 0 || answer >= n);
		cin.ignore(80, '\n');
		cout << endl;
		return answer;
	}

	
	int Appl::Add()
	{
		try{
			baza *ptr = NULL;
			uchebnoe uch;
			nauchnoe nauch;
			hudozh hud;
			int shifr;
			int ans;
			while (ans = Answer(Sh, NumSh)){
				cout << "������� ����: --> ";
				cin >> shifr;
				switch (ans){
				case 1:
					cout << "Enter �� ��������: --> ";
					ptr = &uch;
					break;
				case 2:
					cout << "Enter �� ��������: --> ";
					ptr = &nauch;
					break;
				case 3:
					cout << "Enter �� ���������������: --> ";
					ptr = &hud;
					break;
				}
				cin >> (*ptr);
				cin.ignore(80, '\n');
				tb.insert(shifr, ptr);
				//tb[shifr] = ptr->clone();
				cout << endl;

			}
			return 0;
		}
		catch (exception) {
			cout << "������! " << endl << endl;
			return 0;
		}
	}

	int Appl::udal(){
		try{
			int shifr;
			cout << "����: --> ";
			cin >> shifr;
			cout << endl;
			tb.erase(shifr);
			cout << "������� � ������ " << shifr << " ������" << endl  << endl;
			return 0;
		}
		catch (exception) {
			cout << "������! " << endl << endl;
			return 0;
		}

	}


	int Appl::Find()
	{
		try{
			int shifr;
			const baza *ptr = NULL;
			cout << "����: --> ";
			cin >> shifr;
			cout << endl;

			Mymap<int, baza *>::Iterator it = tb.find(shifr);
			if (it == tb.end()){
				cout << "����� " << shifr << " ���"
					<< endl << endl;
				return -1;
			}
			ptr = (*it).second;
			cout << "���� " << (*it).first << " ��� " << Names[ptr->type()] << " �������;"
				<< endl;
			cout << (*ptr) << endl;

			cout << endl;

			return 0;
		}
		catch (exception) {
			cout << "������! " << endl << endl;
			return 0;
		}
	}



	ostream & operator <<(ostream &os, const Pair<int, baza *> &p)
	{
		cout << endl;
		if (p.second)
			os  << p.first << endl;
		if (p.second)
			os  << (*(p.second));
		//else
		//	os << "is empty";
		cout << endl;
		return os;
	}


	int Appl::ShowAll()
	{
		Mymap<int, baza *>::Iterator p;

		for (p = tb.begin(); p != tb.end(); ++p)
			if ((*p).first!=NULL)
				cout << (*p) << endl;
		cout << endl;

		return 0;
	}

	int Appl::Iter()
	{
		cout << "���������� ��� ���� ������: " << endl;
		cout << endl;

		Mymap<int, baza *>::Iterator it;
		Pair<int, baza *> p;

		baza *pSh = NULL;
		for (it = tb.begin(); it != tb.end(); ++it){
			p = *it;

			if (p.second != NULL)
			{
				pSh = (*it).second;
			int type = pSh->type();
			cout << "������� " << Names[type] << "(����: " << (*it).first << "): " << endl;
			//string *mas = pSh->get_nazvaniya_kursov();

			cout << "���������� �������: " << pSh->getkolvo() << endl;

			switch (type){

			case 1: cout << "�������� �����: " << pSh->getnazvanie() << endl;
				break;

			case 2: cout << "�������� �����: " << pSh->getnazvkur() << endl;
		
				/*for (int i = 0; i < 3; i++){            
					cout << mas[i] << "  ";
				}
				*/
				break;

			case 3:cout << "����: " << pSh->get_tema() << endl;
				break;

			}
			cout << endl;
			cout << endl;
			}
		}
		cout << endl;
		cout << endl;

		return 0;
	}




	int Appl::vidacha(){
		try
		{
			int shifr;
			cout << "����: --> ";
			cin >> shifr;
			cout << endl;
			Mymap<int, baza *>::Iterator it;

			it = tb.find(shifr);
			tb[shifr]->setkolvo(tb[shifr]->getkolvo() - 1);
			return 0;
		}
		catch (exception){
			cout << "������! " << endl << endl;
			return 0;
		}

	}

	int Appl::vozvrat(){
		try{
			int shifr;
			cout << "����: --> ";
			cin >> shifr;
			cout << endl;
			Mymap<int, baza *>::Iterator it;
			tb.find(shifr);
			tb[shifr]->setkolvo(tb[shifr]->getkolvo() + 1);
			return 0;
		}
		catch (exception) {
			cout << "������! " << endl << endl;
			return 0;
		}
	}


	int Appl::SaveFile()
	{
		ofstream f;
		f.open(filename, ios::binary);
		Mymap<int, baza *>::Iterator it;
		for (it = tb.begin(); it != tb.end(); ++it)
		{
			f << (*it).second->type()<<endl;
			f << (*it) << endl;
		}
		f.close();
		return 0;
	}

	int Appl::LoadFile()
	{
		        ifstream f(filename);
		        if (!f)
		        {
		            cout << "Error!" << endl;
		            return 0;
		        }

				while (!f.eof()){
					baza *ptr;
					uchebnoe uch;
					nauchnoe nauch;
					hudozh hud;

					int type, shifr, year, kolvo, ind_gr;
					string fio, nazvanie, izdatelstvo, tema, nazvanie_kursa;

					f >> type;
					switch (type){
					case 1:
						f >> shifr;
						f >> fio;
						f >> nazvanie;
						f >> year;
						f >> izdatelstvo;
						f >> kolvo;
						f >> nazvanie_kursa;
						f >> ind_gr;
						
						uch.setfio(fio);
						uch.setizd(izdatelstvo);
						uch.setkolvo(kolvo);
						uch.setname(nazvanie_kursa);
						uch.setnazvanie(nazvanie);
						uch.setyear(year);
						uch.izm2(ind_gr);
						ptr = &uch;
						break;
					case 2:
						f >> shifr;
						f >> fio;
						f >> nazvanie;
						f >> year;
						f >> izdatelstvo;
						f >> kolvo;
						f >> nazvanie_kursa;

						nauch.setfio(fio);
						nauch.setizd(izdatelstvo);
						nauch.setkolvo(kolvo);
						nauch.setnazvanie(nazvanie);
						nauch.setyear(year);
						nauch.setnazvkur(nazvanie_kursa);
						ptr = &nauch;
						break;
					case 3:
						f >> shifr;
						f >> fio;
						f >> nazvanie;
						f >> year;
						f >> izdatelstvo;
						f >> kolvo;
						f >> tema;

						hud.settema(tema);
						hud.setfio(fio);
						hud.setizd(izdatelstvo);
						hud.setkolvo(kolvo);
						hud.setnazvanie(nazvanie);
						hud.setyear(year);
						ptr = &hud;
						break;
					}
					tb.insert(shifr, ptr);
					//tb[shifr] = ptr->clone();
					cout << endl;
					tema.clear();
					fio.clear();
					izdatelstvo.clear();
					kolvo = NULL;
					nazvanie.clear();
					year = NULL;
					shifr = NULL;
					ind_gr = NULL;
					nazvanie_kursa.clear();
				}

		return 0;
	}




	int findmas(string mas[10], string stroka)
	{
		for (int i = 0; i < 10; i++)
		{
			if (mas[i] == stroka)
				return 1;
		}
		return 0;
	}

	int Appl::nazv_kursov()
	{
		cout << "���������� ��� ���� ������: " << endl;
		cout << endl;
		Mymap<int, baza *>::Iterator it;

		Pair<int, baza*> p;
		baza *pSh = NULL;
		cout << "��������� ����� ���������� �����������: " << endl;
		int j = 0;
		string massiv[10];

		for (it = tb.begin(); it != tb.end(); ++it){
			p = *it;

			if (p.second != NULL)
			{
				pSh = (*it).second;
			int type = pSh->type();
//			string *mas = pSh->get_nazvaniya_kursov();
			switch (type){

			case 1: if (findmas(massiv, pSh->getnazvanie()) == 0)
			{
						massiv[j] = pSh->getnazvanie();
						j++;
			}
					//cout << pSh->getnazvanie() << endl;
					break;

			case 2:
				for (int i = 0; i < 3; i++){            
					if (findmas(massiv, pSh->getnazvkur()) == 0)
					{
						massiv[j] = pSh->getnazvkur();
						j++;
					}

					//	cout << mas[i] << endl;
				}
				break;

			case 3:	break;

			}


		}
		}
		for (int k = 0; k < 10; k++)
		{
				cout << massiv[k] << endl;
		}
	
		return 0;
	}


