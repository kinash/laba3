#ifndef UCHEBNOE_H
#define UCHEBNOE_H

#include "baza.h"

/**
����� ������� �������
 */
class uchebnoe : public baza{
private:
	string nazvanie_kursa;		//�������� �����
	int ind_grupp;			//������ ������
protected:
	/**
	����� ���������� �� ������� �������
	*/
	ostream& show(ostream&) const;
	/**
	���� ���������� �� �������
	*/
	istream& get(istream&);
public:
	/**
	������ ���������������� �����������
	*/
	uchebnoe() :baza(), ind_grupp(NULL){ nazvanie_kursa.clear(); };

	/**
	����������� � �����������
	*/

	uchebnoe(const uchebnoe &uch){
		setfio(uch.getfio());
		setnazvanie(uch.getnazvanie());
		setyear(uch.getyear());
		setizd(uch.getizd());
		setkolvo(uch.getkolvo());

		nazvanie_kursa = uch.nazvanie_kursa;
		ind_grupp = uch.ind_grupp;
	}
	/**
	������������� �������� ������������
	*/
	uchebnoe& operator = (const uchebnoe &uch){
//		delete[]ind_grupp;
		setfio(uch.getfio());
		setnazvanie(uch.getnazvanie());
		setyear(uch.getyear());
		setizd(uch.getizd());
		setkolvo(uch.getkolvo());

		nazvanie_kursa = uch.nazvanie_kursa;
		ind_grupp = uch.ind_grupp;
	}
	/**
	�������� �������� ������
	*/
	string get_nazvanie_kursa()const{ return 0; }
    string getnazvkur()const{ return nazvanie_kursa; }

	/**
	�������� ����
	*/
	string get_tema()const { return 0; }
	/**
	����������� ����� ������������ ������
	*/
	virtual uchebnoe* clone() const
	{
		return new uchebnoe(*this);
	}

	/**
	����������� ���� ������
	*/
	int type()const;
	/**
	�������� ��������
	*/
	string getname()const;
	/**
	�������� ��������
	*/
	void setname(string);
	/**
	�������� �������� �������� �����
	*/
	void izm(int kolvo,const int*);
	void izm2(const int);
	
	/**
	���������� �������� ������
	*/
	virtual ~uchebnoe(){
		nazvanie_kursa.clear();
	}

    virtual string frm() const{                                                             ///�����!
        ostringstream out;
        out<< QString::fromLocal8Bit("��� ������: ").toUtf8().constData()<< getfio()<<endl
           << QString::fromLocal8Bit("��������: ").toUtf8().constData()<<getnazvanie()<<endl
          <<QString::fromLocal8Bit("������������: ").toUtf8().constData()<<getizd()<<endl
        <<QString::fromLocal8Bit("�������� �����: ").toUtf8().constData()<<nazvanie_kursa<<endl
        <<QString::fromLocal8Bit("���: ").toUtf8().constData()<<getyear()<<endl
        <<QString::fromLocal8Bit("���-�� �����������: ").toUtf8().constData()<<getkolvo()<<endl
        <<QString::fromLocal8Bit("������ ������: ").toUtf8().constData()<<ind_grupp<<endl;
        return out.str();
    }


};




#endif
