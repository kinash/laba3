#pragma once

#include <string>
#include <iostream>
#include "baza.h"
#include "mymap.h"

using namespace std;

//����� dialog
class Appl{
private:
	class Funcs{
	public:
		virtual int operator()(Appl &tb) = 0;
	};
	Mymap<int, baza*> tb;  // ��������� map c ������ � ���������� �� ������� �����
	Funcs * fptr[11];	//������ �� ���������� �� �������
public:
	Appl();	//�����������
	~Appl();  //����������
	int run();  //main ������� ��� ��������� � ������
private:
	int Answer(const char *alt[], int n);
	int Add();  //�������� ������� � �������
	int Find();  //����� ������� �� �����
	int ShowAll(); //�������� ���������� �������
	int Iter();  //������� ���������� �� ��������
	int vidacha();  //������ �������
	int vozvrat();  //������� �������
	int udal();  //�������� �������� �� �����
	int SaveFile();  
 	int LoadFile();
	int nazv_kursov();  //������� �������� ������, ������������ �����������

	//����� ����������
	class CAdd :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.Add(); }
	};
	//����� ������
	class CFind :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.Find(); }
	};
	//����� ������ �������
	class CShowAll :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.ShowAll(); }
	};
	//����� ������ ���������� �� ��������
	class CIter :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.Iter(); }
	};
	//����� ����� �������
	class Cvozvrat :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.vozvrat(); }
	};
	//����� ��������
	class Cudal :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.udal(); }
	};

	class CSaveFile :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.SaveFile(); }
	};

	class CLoadFile :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.LoadFile(); }
	};

	//����� ������
	class Cvidacha :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.vidacha(); }
	};

	//����� �������� ������
	class Cnazv_kursov :public Funcs{
	public:
		int operator() (Appl &tb){ return tb.nazv_kursov(); }
	};


};

//�������� ������ ���������� �������
ostream & operator <<(ostream &, const Pair<int, baza *> &);


/**
@author���� Kinash V, K03-123
@version��� 0.9 (beta)
*/

/**
\mainpage 

������� 5. ������� �������.
��������� ���������� ���� ������� ������ ����������.
��� ���������� �� �������(�����) ���������� � ��������� �������. 
������ ������� ����� ���������� ����.
���������� ��� ���� �������� ������� � �������, ������ ������� ������� �������� ���� ������� � ��������� �� ��� ���������.
�������� ������� ����������� �� ������������ �������� ����� �������.

<a href="c:\proga 3 ������\html\classbaza.html" > baza </a> - ������� �����(�����).

<a href="c:\proga 3 ������\html\classhudozh.html" > hudozh </a> - ����� �������������� ������� (����������� �� �����).

<a href="c:\proga 3 ������\html\classnauchnoe.html" > nauchnoe </a>  - ����� ������� ������� (����������� �� �����).

<a href="c:\proga 3 ������\html\classuchebnoe.html" > uchebnoe </a> - ����� ������� ������� (����������� �� �����).

<a href="c:\proga 3 ������\html\class_appl.html" > appl </a>- ����� ������� � ������������� (����������� �� �����).

**/