#ifndef BAZA_H
#define BAZA_H
#include <string>
#include <iostream>
#include <sstream>
#include <QString>

//#include <map>

using namespace std;
using std::string;


/**
������� ����� (�����)

*/
class baza{
private:
	string fio;            // ������� � �������� ������
	string nazvanie;		//�������� �������
	int year;		//���
	string izdatelstvo;		//������������
	int kolvo;		//���������� �����������

protected:
	/**
	����������� ����� ������ ���������� �� �������
	*/
	virtual ostream&show(ostream&)const = 0;
	/**
	����������� ����� ����� ���������� �� �������
	*/

	virtual istream&get(istream&) = 0;
public:
    virtual string frm() const = 0;                                                             ///�����!



	/**
	������ ���������������� �����������
	*/
	baza() :year(0), kolvo(0){
		fio.clear();
		nazvanie.clear();
		izdatelstvo.clear();
	}
	/**
	����������� � �������� �����������
	*/
	baza(string fio1, string nazvanie1, int year1, string izdatelstvo1, int kolvo1);
	/**
	����������� ����� ������������ ������
	*/
	virtual baza* clone()const = 0;
	/**
	������������� �������� ������ � ����� ���������� �� �������
	*/
	friend ostream& operator <<(ostream &os, const baza &p){
		return p.show(os);
	}
	/**
	������������� �������� ����� ���������� �� �������
	*/
	friend istream& operator >>(istream &is, baza &p){
		return p.get(is);
	}
	/**
	�������� ����������
	*/
	int getkolvo() const;
	/**
	�������� ����������
	*/
	void setkolvo(int);
	/**
	�������� ���
	*/
	string getfio()const;
	/**
	�������� ��������
	*/
	string getnazvanie()const;
	/**
	�������� ������������
	*/
	string getizd()const;
	/**
	�������� ���
	*/
	int getyear()const;
	/**
	�������� ���
	*/
	void setfio(string);
	/**
	�������� ��������
	*/
	void setnazvanie(string);
	/**
	�������� ���
	*/
	void setyear(int);
	/**
	�������� ������������
	*/
	void setizd(string);
	/**
	����������� ����� ����������� ���� ������� (�������, �������, ��������������)
	*/
	virtual int type() const = 0;
	/**
	���������� �������� ������
	*/
	virtual ~baza(){
		fio.clear();
		nazvanie.clear();
		izdatelstvo.clear();
		year = 0;
		kolvo = 0;
	}

    virtual string get_tema()const = 0;
	virtual string get_nazvanie_kursa()const = 0;
	virtual string getnazvkur()const = 0;

};

#endif
